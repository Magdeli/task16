﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Task16
{
    class Program
    {
        static void Main(string[] args)
        {
            // Magdeli Holmøy Asplin
            // 8/21/2019

            // This app creates a list of Persons which have a firstname and a lastname.
            // Then it uses the IEnumerable and IEnumerator subclasses PeopleEnum and People
            // to enable the user to go through each Person object and, in this case at least, to
            // print out the names of each person.

            #region Creating the list of Persons

            Person[] peopleArray = new Person[3]
            {
            new Person("Magdeli", "Asplin"),
            new Person("Jostein", "Utkilen"),
            new Person("Reidun", "Holmøy"),
            };

            #endregion


            #region Go through list and print names
            
            People peopleList = new People(peopleArray);
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p.getFirstname() + " " + p.getLastname());
            }

            #endregion


            #region Run queries

            IEnumerable<Person> nameLength =
                from person
                in peopleArray
                where person.getFirstname().Length > 6
                select person;

            foreach(Person person in nameLength)
            {
                Console.WriteLine($"{person.printName()} has a firstname of more than 6 characters .");
            }

            IEnumerable<Person> firstLetter =
                from personM
                in peopleArray
                where personM.getFirstname()[0] == 'M'
                select personM;

            foreach(Person personM in firstLetter)
            {
                Console.WriteLine($"{personM.printName()} has a firstname that starts with an M.");
            }

            #endregion
        }
    }
}
