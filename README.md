# Task16

Magdeli Holmøy Asplin
8/21/2019

This app creates a list of Person objects which have a firstname and a lastname. Then it uses the IEnumerable and IEnumerator
subclasses PeopleEnum and People to enable the user to go through each Person object an print out the full name of each object.
