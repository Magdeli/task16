﻿using System;
using System.Collections;
using System.Text;

namespace Task16
{
    public class People : IEnumerable
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of the IEnumerable. It creates a list of Persons and enables 
        // us to go through the objects one by one using the PeopleEnum class to do so

        #region Creating an enumerable array of Person objects

        private Person[] _people;
        public People(Person[] pArray)
        {
            _people = new Person[pArray.Length];

            for (int i = 0; i < pArray.Length; i++)
            {
                _people[i] = pArray[i];
            }
        }

        #endregion


        #region Implementing GetEnumerator
        // Implementation for the GetEnumerator method.
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public PeopleEnum GetEnumerator()
        {
            return new PeopleEnum(_people);
        }

        #endregion
    }
}
