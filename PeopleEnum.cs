﻿using System;
using System.Collections;
using System.Text;

namespace Task16
{
    public class PeopleEnum : IEnumerator
    {
        // Magdeli Holmøy Asplin
        // 8/21/2019

        // This class is a subclass of the IEnumerator. It tells us how to go through each object
        // in the list of Persons from the People class

        public Person[] _people;

        // Enumerators are positioned before the first element
        // until the first MoveNext() call.
        int position = -1;

        public PeopleEnum(Person[] list)
        {
            _people = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _people.Length);
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Person Current
        {
            get
            {
                try
                {
                    return _people[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

    }
}
